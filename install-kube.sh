# cat > install.sh << EOF
mkdir  ~/kube-cluster

# install package dependencies
apt-get install net-tools git traceroute mtr vim bind9-utils rsync htop wget nmap -y

# ansible
sudo apt-add-repository ppa:ansible/ansible && sudo apt update && sudo apt install ansible -y 

# update package & upgrade
apt update && apt upgrade -y 


# sshd allow
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config && service sshd restart
# create rsa keys
ssh-keygen -t rsa 
ssh-copy-id root@10.16.1.1
ssh-copy-id root@10.16.1.2
ssh-copy-id root@10.16.1.3
ssh-copy-id root@10.16.1.11
ssh-copy-id root@10.16.1.12
ssh-copy-id root@10.16.1.13
@Cappuccino01